import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { UsuarioRepository } from './repository/usuario-repository';
import { UsuarioCadastro } from './dto/usuario-dto';
import { UsuarioEntity } from './entity/usuario-entity';
import { v4 as uuid } from 'uuid';

@Controller('usuario')
export class UsuarioController {
	constructor(
        private repository: UsuarioRepository
    ){}

	@Get()
	getUsuarios() {
		return this.repository.listar()
	}

	@Get(':id')
	getUsuario(@Param() param) {
		return this.repository.search(param.id)
	}

	@Post()
	cadastrarUsuario(@Body() usuario: UsuarioCadastro){
		let usuarioEntity = new UsuarioEntity()

		usuarioEntity.id = uuid()
    usuarioEntity.nome = usuario.nome
    usuarioEntity.email = usuario.email
    usuarioEntity.telefone = usuario.telefone
    usuarioEntity.sexo = usuario.sexo
    usuarioEntity.cep = usuario.cep
    usuarioEntity.logradouro = usuario.logradouro
    usuarioEntity.bairro = usuario.bairro
    usuarioEntity.cidade = usuario.cidade
    usuarioEntity.uf = usuario.uf
    usuarioEntity.ddd = usuario.ddd
    usuarioEntity.duelTime = usuario.duelTime

		this.repository.salvar(usuarioEntity)

		return {
			mensagem: "Dados recebidos",
			data: usuarioEntity
		}
	}

	@Put(':id')
	updateUsuario(@Param() param, @Body() usuario: UsuarioCadastro){
		let usuarioEntity = new UsuarioEntity()

    usuarioEntity.nome = usuario.nome
    usuarioEntity.email = usuario.email
    usuarioEntity.telefone = usuario.telefone
    usuarioEntity.sexo = usuario.sexo
    usuarioEntity.cep = usuario.cep
    usuarioEntity.logradouro = usuario.logradouro
    usuarioEntity.bairro = usuario.bairro
    usuarioEntity.cidade = usuario.cidade
    usuarioEntity.uf = usuario.uf
    usuarioEntity.ddd = usuario.ddd
    usuarioEntity.duelTime = usuario.duelTime

		return this.repository.update(param.id, usuarioEntity)
	}

	@Delete(':id')
	removeNoticia(@Param() param) {
		return this.repository.remove(param.id)
	}
}
