export class UsuarioEntity{
    id: string
    nome: string
    email: string
    telefone: string
    sexo: string
    cep: string
    logradouro: string
    bairro: string
    cidade: string
    uf: string
    ddd: string
    duelTime: number
}
