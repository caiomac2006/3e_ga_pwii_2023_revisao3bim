import { IsEmail, IsNotEmpty, IsNumber, IsString, Min, MinLength } from "class-validator"

export class UsuarioCadastro{
	@IsString({message: "O nome deve ser do tipo string"})
    @IsNotEmpty({message: "O nome não pode ser vazio"})
    @MinLength(2, {message: "o  mínimo de dígitos para o nome é 2"})
	public nome: string

	@IsString({message: "O email deve ser do tipo string"})
    @IsNotEmpty({message: "O email não pode ser vazio"})
    @MinLength(2, {message: "o  mínimo de dígitos para o email é 2"})
	@IsEmail(null, {message: "O email deve ser um email"})
    public email: string

	@IsString({message: "O telefone deve ser do tipo string"})
    @IsNotEmpty({message: "O telefone não pode ser vazio"})
    @MinLength(9, {message: "o  mínimo de dígitos para o telefone é 9"})
    public telefone: string

	@IsString({message: "O sexo deve ser do tipo string"})
    @IsNotEmpty({message: "O sexo não pode ser vazio"})
    @MinLength(8, {message: "o  mínimo de dígitos para o sexo é 8"})
    public sexo: string

	@IsString({message: "O cep deve ser do tipo string"})
    @IsNotEmpty({message: "O cep não pode ser vazio"})
    @MinLength(8, {message: "o  mínimo de dígitos para o cep é 8"})
    public cep: string

	@IsString({message: "O logradouro deve ser do tipo string"})
    @IsNotEmpty({message: "O logradouro não pode ser vazio"})
    @MinLength(2, {message: "o  mínimo de dígitos para o logradouro é 2"})
    public logradouro: string

	@IsString({message: "O bairro deve ser do tipo string"})
    @IsNotEmpty({message: "O bairro não pode ser vazio"})
    @MinLength(2, {message: "o  mínimo de dígitos para o bairro é 2"})
    public bairro: string

	@IsString({message: "A cidade deve ser do tipo string"})
    @IsNotEmpty({message: "A cidade não pode ser vazio"})
    @MinLength(2, {message: "o  mínimo de dígitos para a cidade é 2"})
    public cidade: string

	@IsString({message: "A UF deve ser do tipo string"})
    @IsNotEmpty({message: "A UF não pode ser vazio"})
    @MinLength(2, {message: "o  mínimo de dígitos para o uf é 2"})
    public uf: string

	@IsString({message: "O DDD deve ser do tipo string"})
    @IsNotEmpty({message: "O DDD não pode ser vazio"})
    @MinLength(2, {message: "o  mínimo de dígitos para o DDD é 2"})
    public ddd: string

	@IsNumber(null, {message: "O duelTime deve ser to tipo number"})
	@Min(0, {message: "O duelTime deve ser maior que zero"})
    public duelTime: number
}
