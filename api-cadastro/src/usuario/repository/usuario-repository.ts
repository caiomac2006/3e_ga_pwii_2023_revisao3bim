import { Injectable } from "@nestjs/common"
import { UsuarioEntity } from "../entity/usuario-entity"
import { v4 as uuid } from 'uuid';

@Injectable()

export class UsuarioRepository{
    private _usuarios: UsuarioEntity[]= [
        {
            id: uuid(),
            nome: 'Caio Machado',
            email: 'caiomac2006@gmail.com',
            telefone: '15 996335965',
            sexo: 'masculino',
            cep: '18430-972',
            logradouro: 'Rua São Sebastião 753',
            bairro: 'Campina de Fora',
            cidade: 'Ribeirão Branco',
            uf: 'SP',
            ddd: '15',
            duelTime: 2000
        },
        {
            id: uuid(),
            nome: 'Luigi Fernando',
            email: 'luigi2006@gmail.com',
            telefone: '15 997208601',
            sexo: 'masculino',
            cep: '18430-972',
            logradouro: 'Rua São Sebastião 753',
            bairro: 'Campina de Fora',
            cidade: 'Ribeirão Branco',
            uf: 'SP',
            ddd: '15',
            duelTime: 1999
        },
    ]

    async salvar(usuario: UsuarioEntity){
        this._usuarios.push(usuario)
    }

    async listar(){
        return this._usuarios
    }

    async search(id: string) {
        const usuario: UsuarioEntity = this._usuarios.find((a: UsuarioEntity) => a.id === id)

        return usuario
    }

    private _getIndex(id: string) {
        const index: number = this._usuarios.findIndex((a: UsuarioEntity) => a.id === id)

        return index
    }

    async update(id: string, usuario: UsuarioEntity) {
        let index = this._getIndex(id)

        if ( index < 0 ) {
            return {
                message: "Usuário não encontrado"
            }
        }

        usuario.id = id

        this._usuarios[index] = usuario

        return {
            message: "Usuário atualizado com sucesso",
            data: this._usuarios[index]
        }
    }

    async remove(id: string) {
        let index = this._getIndex(id)

        if ( index < 0 ) {
            return {
                message: "Usuário não encontrado"
            }
        }

        let usuario = this._usuarios.splice(index, 1)

        return {
            message: "Usuário removido com sucesso",
            data: usuario
        }
    }

}
