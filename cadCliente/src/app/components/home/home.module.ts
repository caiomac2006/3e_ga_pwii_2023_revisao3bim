import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTabsModule } from '@angular/material/tabs';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { ClientsListComponent } from './components/clients-list/clients-list.component';
import { ClientDialogComponent } from './components/client-dialog/client-dialog.component';
import { ClientFormComponent } from './components/client-form/client-form.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon'; 
import { MatInputModule } from '@angular/material/input';

@NgModule({
  declarations: [
    HomeComponent,
    ClientsListComponent,
    ClientDialogComponent,
    ClientFormComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    MatFormFieldModule, 
    MatInputModule, 
    MatIconModule,
    MatTabsModule
  ]
})
export class HomeModule { }
