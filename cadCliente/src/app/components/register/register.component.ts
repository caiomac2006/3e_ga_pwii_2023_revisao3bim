import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { Usuario } from 'src/app/models/Usuario';
import { Endereco } from 'src/app/models/Endereco';
import { HttpClient } from '@angular/common/http';
import { BuscarCepService } from 'src/app/services/buscarCep/buscar-cep.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  usuario: Usuario = {
    nome: '',
    email: '',
    telefone: '',
    sexo: '',
    endereco: {
      cep: '',
      logradouro: '',
      bairro: '',
      cidade: '',
      uf: '',
      ddd: ''
    }
  };
  
  @Input() valor: number = 0;

  @Output() mudarValor = new EventEmitter();

  @ViewChild('campoInput') campoValorInput!: HTMLElement;

  adicionar(){
    console.log(this.campoValorInput);
    this.valor++;
    this.mudarValor.emit({novoValor: this.valor});
  }

  retirar(){
    if(this.valor == 0){
      this.valor;
    }
    else{
      this.valor--;
      this.mudarValor.emit({novoValor: this.valor});
    }
  }

  constructor(
    private cepService: BuscarCepService,
    private http: HttpClient
  ){}

  buscarEndereco() {
    if (this.usuario.endereco.logradouro) {
      const url = `https://viacep.com.br/ws/${this.usuario.endereco.cep}/json/`;
      this.http.get<Endereco>(url).subscribe(
        data => {
          this.usuario.endereco.cep = data.cep;
          this.usuario.endereco.bairro = data.bairro;
          this.usuario.endereco.logradouro = data.logradouro;
          this.usuario.endereco.uf = data.uf;
          this.usuario.endereco.ddd = data.ddd;
        },
        error => {
          console.error('Erro ao buscar endereço:', error);
        }
      );
    }
  }
}
