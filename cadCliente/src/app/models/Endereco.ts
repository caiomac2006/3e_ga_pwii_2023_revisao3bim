export class Endereco{
    cep: string = "";
    logradouro: string = "";
    bairro: string = "";
    cidade: string = "";
    uf: string = "";
    ddd: string = "";
}