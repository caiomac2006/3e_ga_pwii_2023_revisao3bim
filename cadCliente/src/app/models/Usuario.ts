import { Endereco } from "./Endereco";

export class Usuario{
    nome: string = "";
    email: string = "";
    telefone: string = "";
    sexo: string = "";
    endereco: Endereco = new Endereco();
}

    