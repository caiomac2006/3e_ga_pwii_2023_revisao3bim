import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import("./components/register/register-routing.module").then(m => m.RegisterRoutingModule)
  },
  {
    path: "register",
    loadChildren: () => import("./components/register/register.module").then(m => m.RegisterModule)
  },
  {
    path: "**",
    loadChildren: () => import("./components/not-found/not-found.module").then(m => m.NotFoundModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
