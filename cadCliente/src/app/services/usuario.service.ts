import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Usuario } from 'src/app/models/Usuario';


@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  private readonly URL_C = ""
  private readonly URL_L = ""
  private readonly URL = this.URL_C


  constructor(
    private http: HttpClient
  ) { }

  cadastrarUsuario(usuario: Usuario): Observable<any>{
    return this.http.post<any>(`${this.URL}usuario/cadastrar`, usuario)
  }

}
