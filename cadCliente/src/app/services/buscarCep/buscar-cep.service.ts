import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BuscarCepService {
  private readonly UrlCep = "https://viacep.com.br/ws/"

  constructor(
    private readonly http : HttpClient
  ) { }

  buscarCep(cep : string): Observable<any> {
    return this.http.get<any>(`${this.UrlCep}${cep}`)
  }
}
