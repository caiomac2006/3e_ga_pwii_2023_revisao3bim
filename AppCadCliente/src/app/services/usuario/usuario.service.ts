import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  private readonly URL="https://3000-caiomac2006-3egapwii202-9ms1rjs6dxp.ws-us104.gitpod.io"

  constructor(private http:HttpClient) { }

  buscarTodosOsUsuarios(): Observable<any> {
    return this.http.get<any>(`${this.URL}/usuario`)
  }

  buscarUsuarioPorId(id:string):Observable<any> {
    return this.http.get<any>(`${this.URL}/usuario/${id}`)
  }

  cadastrarUsuario(usuario: any): Observable<any>{
    return this.http.post<any>(`${this.URL}/usuario`, usuario)
  }

  editarUsuario(usuario: any, id:string): Observable<any>{
    return this.http.put<any>(`${this.URL}/usuario/${id}`, usuario)
  }

  excluirUsuario(id:string): Observable<any>{
    return this.http.delete<any>(`${this.URL}/usuario/${id}`)
  }
}
