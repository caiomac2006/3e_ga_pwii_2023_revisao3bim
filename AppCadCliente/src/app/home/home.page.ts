import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../services/usuario/usuario.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  usuarios:any[]=[] 
  constructor(
    private service:UsuarioService,
  ) { 
    this.usuarios = []
  }

  ngOnInit() {
    this.buscarUsuarios()
    .subscribe({
      next: (dados) => {
        this.usuarios = dados
        console.log(this.usuarios)
      },
      error: (erro) => {
        console.error(erro)
      }
    })

  }

  buscarUsuarios():Observable<any>{
    return this.service.buscarTodosOsUsuarios()
  }
}
