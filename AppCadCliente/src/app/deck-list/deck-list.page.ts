import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'app-deck-list',
  templateUrl: './deck-list.page.html',
  styleUrls: ['./deck-list.page.scss'],
})
export class DeckListPage implements OnInit {

  
  @Input() valor1: number = 0;
  @Input() valor2: number = 0;
  @Input() valor3: number = 0;  

  @Output() mudarValor = new EventEmitter();

  @ViewChild('campoInput') campoValorInput!: any;

  adicionar1(){
    console.log(this.campoValorInput);
    this.valor1++;
    this.mudarValor.emit({novoValor: this.valor1});
  }

  retirar1(){
    if(this.valor1 == 0){
      this.valor1;
    }
    else{
      this.valor1--;
      this.mudarValor.emit({novoValor: this.valor1});
    }
  }


  adicionar2(){
    console.log(this.campoValorInput);
    this.valor2++;
    this.mudarValor.emit({novoValor: this.valor1});
  }

  retirar2(){
    if(this.valor2 == 0){
      this.valor2;
    }
    else{
      this.valor2--;
      this.mudarValor.emit({novoValor: this.valor2});
    }
  }


  adicionar3(){
    console.log(this.campoValorInput);
    this.valor3++;
    this.mudarValor.emit({novoValor: this.valor3});
  }

  retirar3(){
    if(this.valor3 == 0){
      this.valor3;
    }
    else{
      this.valor3--;
      this.mudarValor.emit({novoValor: this.valor1});
    }
  }

  constructor(){}

  ngOnInit() {
  }

}
